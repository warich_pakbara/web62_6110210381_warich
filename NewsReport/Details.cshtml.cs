using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using NewsReport.Data;
using NewsReport.Models;

namespace NewsReport
{
    public class DetailsModel : PageModel
    {
        private readonly NewsReport.Data.NewsReportContext _context;

        public DetailsModel(NewsReport.Data.NewsReportContext context)
        {
            _context = context;
        }

        public NewsCategory NewsCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            NewsCategory = await _context.NewsCategory.FirstOrDefaultAsync(m => m.NewsCategoryID == id);

            if (NewsCategory == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
