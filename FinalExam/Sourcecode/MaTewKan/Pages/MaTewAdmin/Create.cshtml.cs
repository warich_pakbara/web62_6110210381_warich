using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using MaTewKan.Data;
using MaTewKan.Models;

namespace MaTewKan.Pages.MaTewAdmin
{
    public class CreateModel : PageModel
    {
        private readonly MaTewKan.Data.MaTewKanContext _context;

        public CreateModel(MaTewKan.Data.MaTewKanContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["MaTewCategoryID"] = new SelectList(_context.MaTewCategory, "MaTewCategoryID", "Fullname");
            return Page();
        }

        [BindProperty]
        public MaTew MaTew { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.NewsList.Add(MaTew);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}