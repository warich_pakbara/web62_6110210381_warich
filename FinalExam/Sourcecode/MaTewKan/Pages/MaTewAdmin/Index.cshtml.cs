using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MaTewKan.Data;
using MaTewKan.Models;

namespace MaTewKan.Pages.MaTewAdmin
{
    public class IndexModel : PageModel
    {
        private readonly MaTewKan.Data.MaTewKanContext _context;

        public IndexModel(MaTewKan.Data.MaTewKanContext context)
        {
            _context = context;
        }

        public IList<MaTew> MaTew { get;set; }

        public async Task OnGetAsync()
        {
            MaTew = await _context.NewsList
                .Include(m => m.MatewCat).ToListAsync();
        }
    }
}
