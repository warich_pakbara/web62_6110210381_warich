using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MaTewKan.Data;
using MaTewKan.Models;

namespace MaTewKan.Pages.MaTewAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly MaTewKan.Data.MaTewKanContext _context;

        public DetailsModel(MaTewKan.Data.MaTewKanContext context)
        {
            _context = context;
        }

        public MaTew MaTew { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MaTew = await _context.NewsList
                .Include(m => m.MatewCat).FirstOrDefaultAsync(m => m.MaTewID == id);

            if (MaTew == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
