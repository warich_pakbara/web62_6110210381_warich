using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MaTewKan.Data;
using MaTewKan.Models;

namespace MaTewKan.Pages.MaTewAdmin
{
    public class EditModel : PageModel
    {
        private readonly MaTewKan.Data.MaTewKanContext _context;

        public EditModel(MaTewKan.Data.MaTewKanContext context)
        {
            _context = context;
        }

        [BindProperty]
        public MaTew MaTew { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MaTew = await _context.NewsList
                .Include(m => m.MatewCat).FirstOrDefaultAsync(m => m.MaTewID == id);

            if (MaTew == null)
            {
                return NotFound();
            }
           ViewData["MaTewCategoryID"] = new SelectList(_context.MaTewCategory, "MaTewCategoryID", "MaTewCategoryID");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(MaTew).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MaTewExists(MaTew.MaTewID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool MaTewExists(int id)
        {
            return _context.NewsList.Any(e => e.MaTewID == id);
        }
    }
}
