using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MaTewKan.Data;
using MaTewKan.Models;

namespace MaTewKan.Pages.MaTewAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly MaTewKan.Data.MaTewKanContext _context;

        public DeleteModel(MaTewKan.Data.MaTewKanContext context)
        {
            _context = context;
        }

        [BindProperty]
        public MaTew MaTew { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MaTew = await _context.NewsList
                .Include(m => m.MatewCat).FirstOrDefaultAsync(m => m.MaTewID == id);

            if (MaTew == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MaTew = await _context.NewsList.FindAsync(id);

            if (MaTew != null)
            {
                _context.NewsList.Remove(MaTew);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
