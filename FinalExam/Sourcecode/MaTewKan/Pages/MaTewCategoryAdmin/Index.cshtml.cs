using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MaTewKan.Data;
using MaTewKan.Models;

namespace MaTewKan.Pages.MaTewCategoryAdmin
{
    public class IndexModel : PageModel
    {
        private readonly MaTewKan.Data.MaTewKanContext _context;

        public IndexModel(MaTewKan.Data.MaTewKanContext context)
        {
            _context = context;
        }

        public IList<MaTewCategory> MaTewCategory { get;set; }

        public async Task OnGetAsync()
        {
            MaTewCategory = await _context.MaTewCategory.ToListAsync();
        }
    }
}
