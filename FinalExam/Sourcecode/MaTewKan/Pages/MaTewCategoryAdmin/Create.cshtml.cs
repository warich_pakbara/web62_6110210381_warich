using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using MaTewKan.Data;
using MaTewKan.Models;

namespace MaTewKan.Pages.MaTewCategoryAdmin
{
    public class CreateModel : PageModel
    {
        private readonly MaTewKan.Data.MaTewKanContext _context;

        public CreateModel(MaTewKan.Data.MaTewKanContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public MaTewCategory MaTewCategory { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.MaTewCategory.Add(MaTewCategory);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}