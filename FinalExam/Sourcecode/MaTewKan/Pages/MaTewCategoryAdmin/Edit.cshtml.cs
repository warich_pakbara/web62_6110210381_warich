using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MaTewKan.Data;
using MaTewKan.Models;

namespace MaTewKan.Pages.MaTewCategoryAdmin
{
    public class EditModel : PageModel
    {
        private readonly MaTewKan.Data.MaTewKanContext _context;

        public EditModel(MaTewKan.Data.MaTewKanContext context)
        {
            _context = context;
        }

        [BindProperty]
        public MaTewCategory MaTewCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MaTewCategory = await _context.MaTewCategory.FirstOrDefaultAsync(m => m.MaTewCategoryID == id);

            if (MaTewCategory == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(MaTewCategory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MaTewCategoryExists(MaTewCategory.MaTewCategoryID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool MaTewCategoryExists(int id)
        {
            return _context.MaTewCategory.Any(e => e.MaTewCategoryID == id);
        }
    }
}
