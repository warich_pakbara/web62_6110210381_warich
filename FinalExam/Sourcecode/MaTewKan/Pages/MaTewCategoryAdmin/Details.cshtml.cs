using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MaTewKan.Data;
using MaTewKan.Models;

namespace MaTewKan.Pages.MaTewCategoryAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly MaTewKan.Data.MaTewKanContext _context;

        public DetailsModel(MaTewKan.Data.MaTewKanContext context)
        {
            _context = context;
        }

        public MaTewCategory MaTewCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MaTewCategory = await _context.MaTewCategory.FirstOrDefaultAsync(m => m.MaTewCategoryID == id);

            if (MaTewCategory == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
