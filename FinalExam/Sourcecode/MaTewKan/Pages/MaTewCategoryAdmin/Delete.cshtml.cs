using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using MaTewKan.Data;
using MaTewKan.Models;

namespace MaTewKan.Pages.MaTewCategoryAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly MaTewKan.Data.MaTewKanContext _context;

        public DeleteModel(MaTewKan.Data.MaTewKanContext context)
        {
            _context = context;
        }

        [BindProperty]
        public MaTewCategory MaTewCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MaTewCategory = await _context.MaTewCategory.FirstOrDefaultAsync(m => m.MaTewCategoryID == id);

            if (MaTewCategory == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            MaTewCategory = await _context.MaTewCategory.FindAsync(id);

            if (MaTewCategory != null)
            {
                _context.MaTewCategory.Remove(MaTewCategory);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
