using MaTewKan.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace MaTewKan.Data
{
    public class MaTewKanContext : IdentityDbContext<NewsUser>
    {
        public DbSet<MaTew> NewsList { get; set; }
        public DbSet<MaTewCategory> MaTewCategory { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=MaTewKan.db");
        }
    }
}