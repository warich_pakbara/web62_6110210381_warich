using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace MaTewKan.Models
{
    public class NewsUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
    public class MaTewCategory
    {
        public int MaTewCategoryID { get; set;}
        public string Fullname { get; set; }
        public string Subject { get; set; }
        public string Location { get; set; }
        public string Pricehour { get; set; }
    }
    public class MaTew
    {
        public int MaTewID { get; set; }

        public int MaTewCategoryID { get; set; }
        public MaTewCategory MatewCat { get; set; }

        [DataType(DataType.Date)]
        public string Date { get; set; }
        public string Detail { get; set; }
        public string Status { get; set; }

        public string NewsUserId { get; set; }
        public NewsUser postUser { get; set; }
    }
}